# SPDX-FileCopyrightText: 2023-present Guido Kroon <guido@kroon.email>
#
# SPDX-License-Identifier: GPL-3.0-only
__version__ = "0.0.26"
